#---------------------------------------------------------------------------------
.SUFFIXES:
#---------------------------------------------------------------------------------

# You may need to change this to python3 on some machines.
PYTHON := python
PIPENV := $(PYTHON) -m pipenv
PYTHON_VENV := $(PIPENV) run python
INSTALLER := pyinstaller


PROJECT_NAME := target-game
NAME := Targets
ICON := icon.ico

PYFILES := $(notdir $(wildcard ./*.py))

ifeq ($(OS),Windows_NT)
	export PATH := $(PATH);C:\Windows\System32\downlevel;
	BUILD := out/win_build
else
	BUILD := out/other_build
endif

EXE := $(BUILD)/$(PROJECT_NAME)/$(NAME)

.PHONY: all clean dist environment run

all: environment $(BUILD) $(EXE)

dist: all
	cd $(BUILD) && zip -r $(PROJECT_NAME).zip $(PROJECT_NAME)

clean:
	rm -rf $(BUILD)

run: environment
	$(PYTHON_VENV) main.py

environment:
	$(PYTHON) -m pip install pipenv
	$(PIPENV) install

$(EXE) : $(PYFILES)
	rm -rf $(BUILD)/*
	$(PIPENV) run $(INSTALLER) --onefile --windowed main.py -i $(ICON) -n $(NAME)
	mv -b dist $(PROJECT_NAME)
	cp -r assets $(PROJECT_NAME)
	mv build $(PROJECT_NAME) $(BUILD)

$(BUILD):
	mkdir -p $(BUILD)
