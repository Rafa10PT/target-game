from dataclasses import dataclass

@dataclass
class Button:
	m_btn: tuple
	text: str

class buttons:
	# O indíce do botão atual enquanto o rato ainda está a ser premido. Tem o valor -1 quando nenhum botão ainda se encontra premido. 
	cached_button = -1

	def __init__(self, mouse, button_list):
		self.mouse = mouse
		self.button_list = button_list

	def report_clicked_button(self):
		# Se nenhum botão estiver a ser premido e existir um botão guardado.
		if(self.cached_button != -1 and self.mouse.get_pressed()[0] == False):
			# Se estivermos dentro da caixa do botão, devolvemos o seu índice.
			b = self.button_list[self.cached_button]
			if(self.mouse.get_pos()[1] >= b.m_btn[1] and self.mouse.get_pos()[1] <= b.m_btn[1] + b.m_btn[3]
			and self.mouse.get_pos()[0] >= b.m_btn[0] and self.mouse.get_pos()[0] <= b.m_btn[0] + b.m_btn[2]):
				ret = self.cached_button
				# Limpar o botão guardado.
				self.cached_button = -1
				return ret
			# Caso contrário, não consideramos que se carregou no botão.
			else:
				# Limpar o botão guardado.
				self.cached_button = -1
				return None

		# Circular pelos botões há procura de um clique.
		i = 0
		for b in self.button_list:
			pos = self.mouse.get_pos()
			# Verificar se estamos da caixa do botão.
			if(self.mouse.get_pressed()[0] == True and pos[0] >= b.m_btn[0] and pos[0] <= b.m_btn[0] + b.m_btn[2]):
				if(pos[1] >= b.m_btn[1] and pos[1] <= b.m_btn[1] + b.m_btn[3]):
					# Guardar o botão.
					if(self.cached_button == -1):
						self.cached_button = i
						return None
			i += 1