class hud:
	show_level_message = 0

	show_notify = 0

	def __init__(self, screen, font, time):
		self.screen = screen

		self.indicator_font = font.SysFont("Arial", 50)

		self.level_font = font.SysFont("Arial", 25)

		self.notify_font = font.SysFont("Arial", 10)

		self.time = time

	def print_indicator(self, top, bottom, text):
		sur = self.indicator_font.render(text, False, [0, 0, 0])
		self.screen.blit(sur, [top, bottom])


	def start_level_message(self, cur_level, add_score, add_seconds):
		self.lvtext = "Level " + ("5+" if cur_level > 5 else str(cur_level)) + "! +%d score and +%d seconds." % (add_score, add_seconds)

		self.show_level_message = self.time.get_ticks()

	def update_level_message(self):
		if(self.show_level_message != 0):
			self.update_text(self.show_level_message, 5, self.lvtext)

	def start_notify(self, score, ball_pos):
		self.show_notify = self.time.get_ticks()
		self.nfpos = ball_pos
		self.nftext = "+%d" % score

	def update_notify_message(self):
		if(self.show_notify != 0):
			self.update_text(self.show_notify, 2, self.nftext)

	def update_text(self, timer, deadline, text):
		seconds = (self.time.get_ticks() - timer) / 1000
		
		if deadline == 5:
			sur = self.level_font.render(text, False, [0, 0, 0])
			self.screen.blit(sur, [0, 500])
			if(seconds >= deadline): self.show_level_message = 0

		else:
			sur = self.notify_font.render(text, False, [0, 0, 0])
			self.screen.blit(sur, self.nfpos)
			if(seconds >= deadline): self.show_notify = 0
