import pygame
import math
import time
import ui
from buttons import Button, buttons
from random import random
from pygame.draw import rect as draw_rect, line as draw_line
from pygame.image import load as img_load
from pygame.math import Vector2
from pygame.mixer import Sound
from pygame.mouse import get_pressed as mouse_get_pressed, get_pos as mouse_get_pos
from pygame.transform import scale
from hud import hud

def vector2_calc_dist_mouse(a):
	dx = mouse_get_pos()[0] - a.x
	dy = mouse_get_pos()[1] - a.y
	return math.sqrt(dx * dx + dy * dy)

class in_game:

	is_game_playing = False

	are_targets_drawn = False

	cur_level = 0

	ball_vel = Vector2(0, 0)

	air_accel = 0.005

	ball_vel_calc = Vector2(0, 0)

	triple = 0

	prev_secs = 0

	score = 0

	targets = []

	was_hit = False

	is_paused = False

	is_muted = False

	# Há alvos de raio 34.5, 24.5 (~70% do tamanho inicial) e 13.5 (~40% do tamanho inicial).
	targets_radiuses  = [34.5, 24.5, 13.5]

	targets_offsets = [
		targets_radiuses[0] - targets_radiuses[0],
		targets_radiuses[0] - targets_radiuses[1],
		targets_radiuses[0] - targets_radiuses[2]
	]

	# Raio do meio.
	mid_dists_center = [targets_radiuses[0] * (8.28/34.5), targets_radiuses[1] * (8.28/34.5), targets_radiuses[2] * (8.28/34.5)]

	# Posição do rato guardada.
	cached_mouse_pos = 0

	def __init__(self, game_ui):
		# Objeto para acedermos à interface gráfica.
		self.m_ui = game_ui

		self.width = game_ui.width
		self.height = game_ui.height
		self.field_height = game_ui.height - 40

		#self.ball_radius = round((game_ui.width * game_ui.height) / (60000))
		self.ball_radius = 8

		#check_dists = [38, 27.5, 17.5]

		# Raio do alvo + raio da bola (ambos exclusivé pixel do centro).
		self.check_dists = [self.targets_radiuses[0] + self.ball_radius, self.targets_radiuses[1] + self.ball_radius, self.targets_radiuses[2] + self.ball_radius]

		self.ball_pos = Vector2(game_ui.width / 2, game_ui.height - 100)

		self.icon_dim = [28, 28]
		self.icon_pos = Vector2(20, game_ui.height - self.icon_dim[0] - 5)

		# Construir os botões do menu.
		vec = self.icon_pos + Vector2(self.icon_dim[0] + 17, 0)
		self.buttons = [
			# Start Game
			Button([self.icon_pos.x - 3, self.icon_pos.y - 3, self.icon_dim[0] + 6, self.icon_dim[1] + 6], "Start Game"),

			# Records
			Button([vec.x, vec.y, self.icon_dim[0] + 6, self.icon_dim[1] + 6], "Records")
		]

		self.m_buttons = buttons(pygame.mouse, self.buttons)

		self.screen = game_ui.screen

		self.m_hud = hud(game_ui.screen, pygame.font, pygame.time)

		self.font = pygame.font.SysFont("Arial", 50)

		pause_font = pygame.font.SysFont("Verdana", 75)
		pause_text = "PAUSE"
		#self.pause_sur = pause_font.render(pause_text, False, [0x33, 0x33, 0xCC])
		#self.pause_sur = pause_font.render(pause_text, False, [0x24, 0x24, 0x8F])
		self.pause_sur = pause_font.render(pause_text, False, [0xFF, 0xFF, 0x1A])
		self.pause_loc = Vector2((game_ui.width - pause_font.size(pause_text)[0])/ 2, game_ui.height / 4)

		self.line_pos = Vector2(0, self.field_height)
		self.line_dim = Vector2(self.width, self.field_height)

		self.load_images()

		self.load_sfx()

		# Quantidade inicial de ticks.
		self.start_ticks = pygame.time.get_ticks()

	def load_images(self):
		## Icons do jogo propriamente dito.

		# Surface para a imagem do alvo.
		target_img_src = img_load("assets/image/target_red.png")

		dimensions = [
			[int(self.targets_radiuses[0] * 2), int(self.targets_radiuses[0] * 2)],
			[int(self.targets_radiuses[1] * 2), int(self.targets_radiuses[1] * 2)],
			[int(self.targets_radiuses[2] * 2), int(self.targets_radiuses[2] * 2)],
		]
		self.target_red_img = [scale(target_img_src, dimensions[0]), scale(target_img_src, dimensions[1]), scale(target_img_src, dimensions[2])]
		target_img_src = img_load("assets/image/target_blue.png")
		self.target_blue_img = [scale(target_img_src, dimensions[0]), scale(target_img_src, dimensions[1]), scale(target_img_src, dimensions[2])]
		target_img_src = img_load("assets/image/target_yellow.png")
		self.target_yellow_img = [scale(target_img_src, dimensions[0]), scale(target_img_src, dimensions[1]), scale(target_img_src, dimensions[2])]

		# Surface para a imagem da bola.
		self.ball_img = scale(img_load("assets/image/ball.png"), [self.ball_radius * 2, self.ball_radius * 2])

		## Icons do mini-menu do jogo.

		# Surface para a imagem do som.
		self.sound_img = scale(img_load("assets/image/sound.png"), self.icon_dim)

		# Surface para a imagem de sem som.
		self.no_sound_img = scale(img_load("assets/image/no_sound.png"), self.icon_dim)

		# Surface para a imagem de pause.
		self.pause_img = scale(img_load("assets/image/pause.png"), self.icon_dim)

		# Surface para a imagem de play.
		self.play_img = scale(img_load("assets/image/play.png"), self.icon_dim)

	def load_sfx(self):
		self.target_breaking = Sound("assets/sound/target_breaking.wav")

		self.a_triple = Sound("assets/sound/oh_baby_a_triple.wav")

		self.oh_yeah = Sound("assets/sound/oh_yeah.wav")

		self.next_level = Sound("assets/sound/next_level.wav")

	def vector2_calc_dist_target(self, obj, a):
		dx = obj[0][0] + self.targets_radiuses[obj[2]] - a.x
		dy = obj[0][1] + self.targets_radiuses[obj[2]] - a.y
		# Watch hitbox.
		#pygame.draw.circle(self.screen, [0, 0x50, 0xFF, 0], [int(obj[0][0] + self.targets_radiuses[obj[2]]), int(obj[0][1] + self.targets_radiuses[obj[2]])], round(self.check_dists[obj[2]]))
		return math.sqrt(dx * dx + dy * dy)

	# https://github.com/kalaorav/Bomberman/blob/612f5904bdfd374214d7795f44512e3d7b8adfaa/serge/blocks/visualeffects.py
	def darken_screen(self, amount):
	    """Darken a surface"""
	    mask = pygame.surface.Surface([self.width, self.height])
	    mask.fill([0xFF - amount, 0xFF - amount, 0xFF - amount, 0xFF - amount])
	    self.screen.blit(mask, [0, 0], special_flags = pygame.BLEND_RGB_MULT)

	def start_game(self):
		result = self.m_buttons.report_clicked_button()
		sound_button_img = self.sound_img
		if(result == 0):
			self.is_paused = not self.is_paused
			if(self.is_paused == True):
				self.pause_ticks = pygame.time.get_ticks()
				self.darken_screen(170)
				self.screen.blit(self.pause_sur, self.pause_loc)

			elif(self.is_paused == False):
				self.start_ticks += pygame.time.get_ticks() - self.pause_ticks

		elif(result == 1):
			self.is_muted = not self.is_muted

		if(not self.is_paused):
			# Falso significa saída.
			if(self.on_field_draw_action() == False):
				return None
		else:
			# Não desenhar sobre o campo para manter a imagem.
			draw_rect(self.screen, [0x64, 0xF7, 0x6F, 0x55], [0, self.height - 38, self.width, 38])

		#pygame.draw.circle(self.screen, [0, 0, 0, 0], [int(self.ball_pos[0]), int(self.ball_pos[1])], 1)

		self.screen.blit(self.pause_img if self.is_paused else self.play_img, self.icon_pos)
		draw_rect(self.screen, [0, 0, 0, 0], [self.icon_pos.x - 3, self.icon_pos.y - 3, self.icon_dim[0] + 6, self.icon_dim[1] + 6], 1)

		vec = self.icon_pos + Vector2(self.icon_dim[0] + 20, 0)
		self.screen.blit(self.no_sound_img if self.is_muted else self.sound_img, vec)
		draw_rect(self.screen, [0, 0, 0, 0], [vec.x - 3, vec.y - 3, self.icon_dim[0] + 6, self.icon_dim[1] + 6], 1)

		# Atualizar exibição com o ecrã no qual desenhámos.
		pygame.display.update()


	def on_field_draw_action(self):
		# Cor verde no ecrã.
		self.screen.fill([0x64, 0xF7, 0x6F, 0x55])

		draw_line(self.screen, [0x13, 0xC, 0x33, 0xFF], self.line_pos, self.line_dim, 2)

		##IF## DEBUG_BALL
		# self.debug_ball = False
		# if(mouse_get_pressed()[0] == False):
			# if(mouse_get_pressed()[2] == True):
				# self.reset_ball()
			# if(mouse_get_pressed()[1] == True):
				# self.start_ticks = -400000
		# else:
			# if(mouse_get_pressed()[2] == True):
				# self.ball_pos = Vector2(mouse_get_pos()[0], mouse_get_pos()[1])
				# self.debug_ball = True
		# 
		# if(not self.debug_ball):
# 
			# self.calc_launch()
			# 
			# if(self.calc_timer() == 0): return False
# 
			# if(self.ball_vel.x != 0 or self.ball_vel.y != 0):
				# self.calc_ball_pos()
# 
		##ELSE##
		if(mouse_get_pressed()[2] == True):
			self.reset_ball()
		if(mouse_get_pressed()[1] == True):
			self.start_ticks = -400000
		

		self.calc_launch()
		
		if(self.calc_timer() == 0): return False

		if(self.ball_vel.x != 0 or self.ball_vel.y != 0):
			self.calc_ball_pos()
		##ENDIF## DEBUG_BALL

		# Desenhar os alvos.
		vel = self.ball_vel.magnitude()
		for t in self.targets:
			dist = self.vector2_calc_dist_target(t, self.ball_pos)
			if(dist > self.check_dists[t[2]] or vel > 0.03
			##IF## DEBUG_BALL
			# or self.debug_ball
			##ENDIF## DEBUG_BALL
			):
				img = self.target_red_img if t[1] == 1 else self.target_blue_img if t[1] == 2 else self.target_yellow_img
				self.screen.blit(img[t[2]], t[0])
				#pygame.PixelArray(self.screen)[t[0][0], t[0][1]] = 0x0
				#draw_rect(self.screen, [0, 0, 0, 0], [t[0][0], t[0][1], 2 * (check_dists[t[2]] - 3.5), 2 * (check_dists[t[2]] - 3.5)], 2)

			else:
				self.was_hit = True

				self.ball_vel = Vector2(0, 0)

				#self.score += 10 if dist < 8.28 else 5

				if(not self.is_muted): self.target_breaking.play()

				if(dist < self.mid_dists_center[t[2]]):
					self.score += 10 * t[1]
					self.m_hud.start_notify(10 * t[1], self.ball_pos)
					if(self.triple == 2):
						self.triple = 0
						if(not self.is_muted): self.a_triple.play()
					else:
						self.triple += 1

				else:
					self.score += 5 * t[1]
					self.m_hud.start_notify(5 * t[1], self.ball_pos)
					self.triple = 0

				self.check_level_up()
				
				time.sleep(1)

				self.targets.remove(t)


		self.m_hud.update_level_message()
		self.m_hud.update_notify_message()

		text = "%02d" % self.score
		self.m_hud.print_indicator(self.width - 30 - self.font.size(text)[0], self.height - 100, text)

		# Desenhar a bola no lugar atual. Puxar para a esquerda e para cima com raios impares.
		self.screen.blit(self.ball_img, [self.ball_pos.x - round(self.ball_radius), self.ball_pos.y - round(self.ball_radius)])

	def hud_arrow_update(self):
		faria = self.ball_vel_calc.magnitude()
		power = faria / 9
		print ("power: %d " % power)
			
		if(power <= 2):
			color = [0xFF,0xFF,0xFF,0xFF]
		elif(power <= 4):
			color = [0xFF,0xFF,0x33,0xFF]
		elif(power <= 6):
		    color = [0xFF,0x99,0x00,0xFF]
		elif(power <= 8):
			color = [0xFF,0x00,0x00,0xFF]
		elif(power <= 10):
			color = [0xFF,0x00,0x99,0xFF]

		draw_line(self.screen, color, self.ball_pos, self.ball_pos - self.ball_vel_calc, 3)
		draw_line(self.screen, color, self.ball_pos - self.ball_vel_calc, (self.ball_pos -(0.8* self.ball_vel_calc).rotate(15)), 3)
		draw_line(self.screen, color, self.ball_pos - self.ball_vel_calc, (self.ball_pos -(0.8* self.ball_vel_calc).rotate(-15)), 3)

	def check_level_up(self):
		if((self.score >= 20 and self.cur_level == 0)
		or (self.score >= 60 and self.cur_level == 1)
		or (self.score >= 120 and self.cur_level == 2)
		or (self.score >= 200 and self.cur_level == 3)
		or (self.score >= 310 and self.cur_level == 4)
		or (self.score >= 400 and (self.score % 100) < 10)
		):
			add_seconds = self.cur_level * 5 + 20 if self.cur_level < 5 else 45
			add_score = self.cur_level * 5 + 10 if self.cur_level < 5 else 35
			self.cur_level += 1

			self.score += add_score
			self.start_ticks += add_seconds * 1000
			self.prev_secs -= add_seconds

			self.m_hud.start_level_message(self.cur_level, add_score, add_seconds)

			if(not self.is_muted): (self.oh_yeah if int(random() * 2) == 1 else self.next_level).play()

	def reset_ball(self):
		##IF## DEBUG_BALL
		# if(not self.debug_ball):
			# if(self.ball_pos != Vector2(400, 570-30)):
				# print(self.was_hit)
				# if(not self.was_hit):
					# self.triple = 0
				# else:
					# self.was_hit = False
			# self.ball_vel = Vector2(0, 0)
			# self.ball_pos = Vector2(400, 540)
		##ELSE##
		if(self.ball_pos != Vector2(400, 570-30)):
			print(self.was_hit)
			if(not self.was_hit):
				self.triple = 0
			else:
				self.was_hit = False
		self.ball_vel = Vector2(0, 0)
		self.ball_pos = Vector2(400, 540)
		##ENDIF## DEBUG_BALL

	def calc_timer(self):
		# Obter o número de segundos com base nos ticks.
		seconds = (pygame.time.get_ticks() - self.start_ticks) / 1000
		# Resolver problema dos alvos não aparecerem por causa do tempo.
		if(not self.are_targets_drawn or seconds - self.prev_secs >= 10):
			self.reset_ball()
			print(seconds - self.prev_secs)
			self.calc_targets()
			self.prev_secs = seconds


		self.m_hud.print_indicator(30, self.height - 100, "%02d" % round(100 - seconds))

		if(seconds >= 100):
			draw_rect(self.screen, [0x64, 0xF7, 0x6F, 0x55], [0, 530, 800, 70])
			draw_rect(self.screen, [0, 0, 0, 0], [self.m_ui.top_rect, self.m_ui.bot_rect, self.m_ui.rect_width, self.m_ui.rect_height], 2)
			self.m_ui.start_leaderboards(True, self.score)
			self.m_ui.page = 2
			del self.m_ui.game
			return 0

	def calc_launch(self):
		# Verificar rato.
		if(mouse_get_pressed()[0] == True and self.ball_vel.magnitude() == 0):
			if(vector2_calc_dist_mouse(self.ball_pos if self.cached_mouse_pos == 0 else self.cached_mouse_pos) <= 90):
				# Guardar posição inicial caso o objeto ainda não esteja inicializado.
				if(self.cached_mouse_pos == 0):
					self.cached_mouse_pos = Vector2(mouse_get_pos()[0], mouse_get_pos()[1])
					return None
				# Ir atualizando a futura velocidade com base nas coordenadas com o arrastamento do rato.
				cur_pos = mouse_get_pos()
				self.ball_vel_calc.x = cur_pos[0] - self.cached_mouse_pos.x
				self.ball_vel_calc.y = cur_pos[1] - self.cached_mouse_pos.y
				#5 *

				self.hud_arrow_update()
				
				print("VelX calc: %.2f" % self.ball_vel_calc.x)
				print("VelY calc: %.2f" % self.ball_vel_calc.y)
				return None

			self.hud_arrow_update()

		# Ao ser largado o rato, podemos determinar o vetor velocidade final
		elif(self.cached_mouse_pos != 0 and mouse_get_pressed()[0] == False):
			print("A")
			self.cached_mouse_pos = 0
			# Constante pela qual dividimos para abrandarmos um pouco e obtermos o sentido vetorial correto.
			self.ball_vel_calc /= -25.0
			self.ball_vel = self.ball_vel_calc


	def calc_ball_pos(self):
		print("VelX: %.2f" % self.ball_vel.x)
		print("VelY: %.2f" % self.ball_vel.y)


		angle = math.atan2(self.ball_vel.y, self.ball_vel.x)
		self.ball_vel.x -= self.air_accel * math.cos(angle)
		self.ball_vel.y -= self.air_accel * math.sin(angle)

		# Calcular posição da bola.
		new_pos = self.ball_pos + self.ball_vel

		# Tocando nos limites verticais ou horizontais, há inversão de sentido da componente vetorial respetiva.
		if(new_pos.x < 0 or new_pos.x > 800):
			self.ball_vel.x = -self.ball_vel.x
			new_pos.x = self.ball_pos.x + self.ball_vel.x

		if(new_pos.y < 0 or new_pos.y > 600):
			self.ball_vel.y = -self.ball_vel.y
			new_pos.y = self.ball_pos.y + self.ball_vel.y

		# Atribuir a nova posição determinada.
		self.ball_pos = new_pos

		print("PosX: %.2f" % self.ball_pos.x)
		print("PosY: %.2f" % self.ball_pos.y)



	def calc_targets(self):
		a = random() * 20
		if(a < 1):
			amount = 5
		elif(a < 3):
			amount = 6
		elif(a < 6):
			amount = 7
		elif(a < 10):
			amount = 8
		elif(a < 14):
			amount = 9
		elif(a < 17):
			amount = 10
		elif(a < 19):
			amount = 11
		elif(a < 20):
			amount = 12

		# Percorrer o ecrã e desenhar alvos com espaço de 10 pixeis entre eles.
		matrix_check = [None] * 60
		self.targets = []

		print(a)
		print(amount)

		i = 0
		while i < amount:
			while True:
				a = int(random() * 60)
				if(matrix_check[a] == None):
					print(matrix_check[a])
					matrix_check[a] = True
					print(a)
					print(int(a % 10))
					print(int(a / 10))
					rand = random() * 100
					print("num;")
					print(rand)
					red = 15 + (amount - 5) * 10
					blue = 45 - (amount - 5) * 4.5 + red
					print(1 if rand <= red else 2 if rand <= blue else 3)
					print(red)
					print(blue)

					if(self.cur_level < 3):
						_100psize = 100
						_70psize = 0
					else:
						_100psize = 100 - self.cur_level * 5
						_70psize = (100 if self.cur_level < 5 else 100 - (self.cur_level - 4) * 5)

					rand = random() * 100

					num = 0 if rand < _100psize else 1 if rand < _70psize else 2 
					num = 0
					self.targets.append([[10 + (a % 10) * 79 + self.targets_offsets[num], int(a / 10) * 79 + self.targets_offsets[num]], 1 if rand < red else 2 if rand < blue else 3, num])
					break
			i += 1

		#self.targets = []
		#i = 0
		#while i < 60:
			#sizes = [79, 89.5, 99.5]
			#rads = [0, 10.35, 20.7]
			#num = int(random() * 3)
			#num = 2
			#self.targets.append([[10 + int(i % 10) * 79 + rads[num], int(i / 10) * 79 + rads[num]], 1, num])
			#i += 1

		self.are_targets_drawn = True
