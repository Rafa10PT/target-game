import pdb
import pygame
import pickle
import os
from pygame.draw import rect as draw_rect
from dataclasses import dataclass

@dataclass
class RankEntry:
	name: str
	score: int

def is_file_empty_2(file_name):
	""" Check if file is empty by confirming if its size is 0 bytes"""
	# Check if file exist and it is empty
	return os.path.isfile(file_name) and os.path.getsize(file_name) == 0

class leaderboards:
	text = ""

	# Lista do ranking nas leaderboards.
	ranks = []

	were_res_drawn = False

	def __init__(self, game_ui, screen):
		self.m_ui = game_ui
		self.screen = game_ui.screen

		self.font = pygame.font.SysFont("Arial", 23)
		self.save_status = 0
		self.pos = 0
		self.pressed = False
		self.is_file_new = False

		self.victory_sound = pygame.mixer.Sound("assets/sound/congratulations.wav")
		self.defeat_sound = pygame.mixer.Sound("assets/sound/try_again.wav")

		draw_rect(self.screen, [0x13, 0xC, 0x33, 0xFF], [0, game_ui.height - 40, game_ui.width, 40])
		sur = pygame.font.SysFont("Arial", 35).render("LEADERBOARDS", False, [0xFF,0xFF,0x33,0xFF])
		screen.blit(sur, [0, game_ui.height - 40])

		# Desenhar retângulo branco.
		draw_rect(self.screen, [0xFF, 0xFF, 0xFF, 0xFF], [game_ui.top_rect + 2, game_ui.bot_rect + 2, game_ui.rect_width - 3, game_ui.rect_height - 3])

		self.top_rect = self.m_ui.top_rect
		self.rect_width = self.m_ui.rect_width
		self.bot_rect = self.m_ui.bot_rect
		self.rect_height = self.m_ui.rect_height

	def check_ranks_init(self):
		if(not self.ranks):
			self.file_name = "save.pickle"
			if(not os.path.isfile(self.file_name) or os.path.getsize(self.file_name) == 0):
				return False
			
			pickle_in = open("save.pickle", "rb")
			
			self.ranks = pickle.load(pickle_in)

			pickle_in.close()

		return True

	def show_ranking(self):		
		left = self.top_rect + round(self.rect_width / 10)

		right = self.top_rect + self.rect_width - round(self.rect_width / 10)

		padding = int((11 * self.rect_height) / 460) + self.font.get_height()
		offset = int((self.rect_height - padding) / 15)
		padding += self.bot_rect #+ (self.rect_height - padding) % 15


		if(self.check_ranks_init() == True):
			i = 0
			# Ascent and descent are added.
			for e in self.ranks:
				textscore = str(e.score)
				rafa = padding + offset * i
				#surface = pygame.font.SysFont("Arial", 23).render(e.name, False, [0, 0, 0])
				#self.screen.blit(surface, [90, 80 + 36 * i])
				surface = self.font.render(e.name, False, [0, 0, 0])
				self.screen.blit(surface, [left, rafa])

				surface2 = self.font.render(textscore, False, [0, 0, 0])
				self.screen.blit(surface2, [right - self.font.size(textscore)[0], rafa])

				self.font.set_underline(True)
				
				surface3 = self.font.render("Name", False, [0, 0, 0])
				self.screen.blit(surface3, [left, self.bot_rect])

				surface4 = self.font.render("Score", False, [0, 0, 0])
				self.screen.blit(surface4, [right - self.font.size("Score")[0], self.bot_rect])

				self.font.set_underline(False)
				
				i += 1

				surface5 = self.font.render(str(i) + '.', False, [0, 0, 0])
				self.screen.blit(surface5, [self.top_rect + 40 - self.font.size(str(i))[0], rafa])

		pygame.display.update()

	def on_exit(self):
		self.m_ui.main_menu_show()
		self.m_ui.page = 0
		del self.m_ui.leaderboards

	def save_game(self):
		self.ranks.insert(self.pos, self.cur_entry)
		if(len(self.ranks) > 15): self.ranks.pop()

		pickle_out = open("save.pickle", "wb")

		pickle.dump(self.ranks, pickle_out)

		pickle_out.close()

	def print_centered(self, text, bot):
		surface = self.font.render(text, False, [0, 0, 0])
		self.screen.blit(surface, [self.top_rect + int((self.rect_width - self.font.size(text)[0]) / 2), bot])

	def new_record(self):
		pos = int((6 * self.rect_height) / 23) + self.bot_rect + self.font.get_height()
		add = int(self.rect_height / 8)


		if(self.save_status == 0):
			# Desenhar retângulo de contorno.
			#draw_rect(self.screen, [0, 0, 0, 0], [70, 70, 660, 460], 2)

			# Centrar e puxar para a esquerda se ímpar.

			self.print_centered("Muito bem!", self.top_rect + 20)
			self.print_centered("A sua pontuação foi: " + str(self.score), pos)
			self.print_centered("Fez uma pontuação que entra no top 15 dos recordes!" if self.pos != 0 else "Parabéns! Novo Recorde!", pos + (self.font.get_height() + add) * 1)
			self.print_centered("Insira o seu nome:", pos + (self.font.get_height() + add) * 2)

			#draw_rect(self.screen, [0, 0, 0, 0], [186, 346, 428, 26], 2)
			#draw_rect(self.screen, [0xFF, 0xFF, 0xFF, 0xFF], [188, 348, 425, 23])
			draw_rect(self.screen, [0, 0, 0, 0], [184, pos + ((self.font.get_height() + add)) * 3, 431, 29], 2)

			pygame.display.update()
		
			self.save_status = 1

		if(self.m_ui.event.type == pygame.KEYDOWN and self.pressed == False):
			if(self.m_ui.event.key != pygame.K_LSHIFT and self.m_ui.event.key != pygame.K_RSHIFT): self.pressed = True
			else: return None
			if(self.m_ui.event.key == pygame.K_RETURN):
				self.cur_entry = RankEntry(self.text, self.score)
				self.save_game()
				self.on_exit()
				return None
			elif(self.m_ui.event.key == pygame.K_BACKSPACE):
				self.text = self.text[:-1]
				draw_rect(self.screen, [0xFF, 0xFF, 0xFF, 0xFF], [186, pos + ((self.font.get_height() + add)) * 3 + 2, 428, 26])
			else:
				if(len(self.text) < 15):
					self.text += self.m_ui.event.unicode

			surface = self.font.render(self.text, False, [0, 0, 0])
			self.screen.blit(surface, [186, pos + ((self.font.get_height() + add) * 3) + 2])

			pygame.display.update()

		elif(self.m_ui.event.type == pygame.KEYUP):
			if(self.m_ui.event.key != pygame.K_LSHIFT and self.m_ui.event.key != pygame.K_RSHIFT):
				self.pressed = False


	def is_new_record(self):
		if(self.check_ranks_init() == False):
			self.pos = 0
			self.victory_sound.play()
			return True

		ranklen = len(self.ranks)
		if(self.ranks and ranklen < 15 and self.score <= self.ranks[-1].score):
			self.pos = ranklen
			self.victory_sound.play()
			return True

		i = 0
		for r in self.ranks:
			if r.score < self.score:
				self.pos = i
				self.victory_sound.play()
				return True
			i += 1

		self.defeat_sound.play()
		return False

	def ranking_init(self, save, score):
		if(pygame.mouse.get_pressed()[2] == True):
			self.on_exit()
			return None

		if(save == True):
			self.score = score
			save = self.is_new_record()

		if(save == True or self.save_status != 0):
			self.new_record()

		elif(self.were_res_drawn == False):
			self.show_ranking()
			self.were_res_drawn = True

#>>> pygame.font.SysFont("Arial", 23).get_height()
#26
#>>> pygame.font.SysFont("Arial", 23).get_descent()
#-4
#>>> pygame.font.SysFont("Arial", 23).get_ascent()
#21
