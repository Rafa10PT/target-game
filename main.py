# Importar tudo da classe ui para podermos ter acesso a partir do ficheiro atual.
import pygame
from ui import ui

# Criar um objeto para a interface gráfica ui.
ui = ui(800, 640)
#ui = ui(1800, 940)
#ui = ui(1800, 1200)

ui.main_menu_show()

# Loop da execução da aplicação.
# Obtemos os eventos que nos permitem detetar, por exemplo,
# teclas primitas, botões do rato, etc..
# Um evento de saída faz com que a aplicação termine. 
mainLoop = True

while mainLoop:
	events = pygame.event.get()
	# Verificar ações do utilizador no menu principal.
	mainLoop = ui.main_menu_handle()
	for event in events:
		ui.event = event
		if event.type == pygame.QUIT:
			mainLoop = False
