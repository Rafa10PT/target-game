import pygame
from pygame.draw import rect as draw_rect
from pygame.image import load as img_load
from pygame.transform import scale

class rules:
	text = ""

	# Lista do ranking nas leaderboards.
	ranks = []

	was_drawn = False

	def __init__(self, game_ui, screen):
		self.m_ui = game_ui
		self.screen = game_ui.screen

		draw_rect(self.screen, [0x13, 0xC, 0x33, 0xFF], [0, game_ui.height - 40, game_ui.width, 40])
		
		sur = pygame.font.SysFont("Arial", 35).render("RULES", False, [0xFF,0xFF,0x33,0xFF])
		screen.blit(sur, [0, game_ui.height - 40])

		# Desenhar retângulo branco.
		draw_rect(self.screen, [0xFF, 0xFF, 0xFF, 0xFF], [game_ui.top_rect + 2, game_ui.bot_rect + 2, game_ui.rect_width - 3, game_ui.rect_height - 3])

		self.top_rect = self.m_ui.top_rect
		self.rect_width = self.m_ui.rect_width
		self.bot_rect = self.m_ui.bot_rect
		self.rect_height = self.m_ui.rect_height

		self.font = pygame.font.SysFont("Arial", 23)

		self.icon_size = int(self.rect_height / 15)
		dim = [self.icon_size, self.icon_size]
		target_img_src = img_load("assets/image/target_red.png")
		self.target_red_img = scale(target_img_src, dim)
		target_img_src = img_load("assets/image/target_blue.png")
		self.target_blue_img = scale(target_img_src, dim)
		target_img_src = img_load("assets/image/target_yellow.png")
		self.target_yellow_img = scale(target_img_src, dim)

		self.sound_imgs = [scale(img_load("assets/image/no_sound.png"), dim), scale(img_load("assets/image/sound.png"), dim)]

		self.pause_play_imgs = [scale(img_load("assets/image/pause.png"), dim), scale(img_load("assets/image/play.png"), dim)]

		self.pos = self.bot_rect
		self.increment = self.font.get_height()

	def on_exit(self):
		self.m_ui.main_menu_show()
		self.m_ui.page = 0
		del self.m_ui.rules

	def print_corner(self, text, offset=0):
		surface = self.font.render(text, False, [0, 0, 0])
		self.screen.blit(surface, [self.top_rect + offset + 2, self.pos])
		self.pos += self.increment

	def game_menu_icons(self, img, text):
		self.screen.blit(img[0], [self.top_rect + 2, self.pos])
		self.screen.blit(img[1], [self.top_rect + self.icon_size + 2, self.pos])
		self.print_corner(text, 2 * self.icon_size)


	def draw_rules(self):
		self.print_corner("O jogo consiste em acertar nos alvos como no Curling.")
		self.print_corner("Primir o botão esquerdo do rato sobre a sua bola e arrastar")
		self.print_corner("o rato para escolher a força.")
		self.print_corner("A orientação da seta mostra a direção da bola.")
		self.print_corner("A orientação da seta mostra a direção da bola.")
		self.print_corner("Primir o botão direito para a bola voltar ao ponto de partida.")
		self.print_corner("Primir o botão do meio para acabar o jogo.")
		self.print_corner("Os alvos têm as seguintes pontuações:")

		self.increment = self.icon_size
		self.screen.blit(self.target_red_img, [self.top_rect + 2, self.pos])
		self.print_corner(" - 5 pontos e 10 se for no meio.", self.icon_size)

		self.screen.blit(self.target_blue_img, [self.top_rect + 2, self.pos])
		self.print_corner(" - 10 pontos e 20 se for no meio.", self.icon_size)
		
		self.screen.blit(self.target_yellow_img, [self.top_rect + 2, self.pos])
		self.print_corner(" - 15 pontos e 30 se for no meio.", self.icon_size)

		self.increment = self.font.get_height()
		self.print_corner("Icones do menu de jogo:")
		self.increment = self.icon_size
		self.game_menu_icons(self.pause_play_imgs, " - Pausar / Retomar jogo.")
		self.game_menu_icons(self.sound_imgs, " - (Des)ligar som.")
		self.print_corner("Menu principal (também nos recordes): botão direito.")



		#self.screen.blit(self.no_sound_img, [self.top_rect + 2, self.bot_rect + self.font.get_height() * 7 + self.icon_size * 2])

	def rules_init(self):
		if(pygame.mouse.get_pressed()[2] == True):
			self.on_exit()
			return None

		if(self.was_drawn == False):

			self.draw_rules()
			
			pygame.display.update()
			
			self.was_drawn = True