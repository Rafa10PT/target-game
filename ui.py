import pygame
import time
from buttons import Button, buttons
from in_game import in_game
from leaderboards import leaderboards
from rules import rules
from pygame.draw import rect as draw_rect

class ui:
	# ID da página em que nos encontramos na interface gráfica. A página 0 é o menu e a 1 é o jogo. 
	page = 0

	# Objeto para a classe do jogo.
	game = 0

	# Objeto para o ranking dos jogadores.
	leaderboards = 0

	# Objeto para as regras.
	rules = 0

	# Evento.
	event = 0

	def __init__(self, width, height):
		pygame.init()

		self.width = width

		self.height = height

		self.screen = pygame.display.set_mode([width, height])

		self.top_rect = round((7 * width) / 80)
		self.rect_width = width - 2 * self.top_rect

		# Espaço para o rodapé.
		height -= 40
		self.bot_rect = int((7 * height) / 60)
		self.rect_height = height - 2 * self.bot_rect

		off_top = int((5 * self.rect_width) / 132)
		off_bot = int((5 * self.rect_height) / 92)
		top_btn = off_top + self.top_rect
		bot_btn = off_bot + self.bot_rect
		btn_width = self.rect_width - 2 * off_top
		btn_height = int((self.rect_height - off_bot * 5) / 4)



		# Construir os botões do menu.
		self.buttons = [
			# Start Game
			Button([top_btn, bot_btn, btn_width, btn_height], "Start Game"),

			# Recordes
			Button([top_btn, bot_btn + btn_height + off_bot, btn_width, btn_height], "Records"),

			# Rules
			Button([top_btn, bot_btn + 2 * (btn_height + off_bot), btn_width, btn_height], "Rules"),

			# Exit
			Button([top_btn, bot_btn + 3 * (btn_height + off_bot), btn_width, btn_height], "Exit")
		]

		self.m_buttons = buttons(pygame.mouse, self.buttons)

		pygame.font.init()

		self.font = pygame.font.SysFont("Arial", 22)

		self.credits_font = pygame.font.SysFont("Arial", 17)

		pygame.display.set_caption("Targets")

	def main_menu_show(self):
		# Cor verde no ecrã todo.
		self.screen.fill([0x64, 0xF7, 0x6F, 0x55])

		# Desenhar retângulo de contorno.
		draw_rect(self.screen, [0, 0, 0, 0], [self.top_rect, self.bot_rect, self.rect_width, self.rect_height], 2)
		
		# Desenhar retângulo branco.
		draw_rect(self.screen, [0xFF, 0xFF, 0xFF, 0xFF], [self.top_rect + 2, self.bot_rect + 2, self.rect_width - 3, self.rect_height - 3])
		
		# Desenhar retângulo dos créditos.
		draw_rect(self.screen, [0x13, 0xC, 0x33, 0xFF], [0, self.height - 40, self.width, 40])
		sur = self.credits_font.render("Made by:", False, [0xFF, 0xFF, 0x33, 0xFF])
		self.screen.blit(sur, [0, self.height - 40])
		sur = self.credits_font.render("Gamog, Valido, Rafa10", False, [0xFF,0xFF,0x33,0xFF])
		self.screen.blit(sur, [0, self.height - 20])

		# Desenhar os botões no ecrã.
		for b in self.buttons:
			# Desenhar o botão atual.
			draw_rect(self.screen, [0, 0, 0, 0], b.m_btn, 2)
			
			# Construir surface com o texto que será desenhado.
			surface = self.font.render(b.text, False, (0, 0, 0))

			# Determinar posição do texto e desenhar.
			self.screen.blit(surface, [b.m_btn[0] + ((b.m_btn[2] - surface.get_width()) / 2), b.m_btn[1] + ((b.m_btn[3] - surface.get_height()) / 2)])
		
		# Atualizar exibição com o ecrã no qual desenhámos.
		pygame.display.update()

	def main_menu_handle(self):
		# Se ainda estivermos no main menu.
		if(self.page == 0):
			btn = self.m_buttons.report_clicked_button()

			if(btn == None):
				return True

			# Se for zero, transicionamos para a página do jogo.
			if(btn == 0):
				self.page = 1
				return True

			if(btn == 1):
				self.page = 2
				return True

			if(btn == 2):
				self.page = 3
				return True

			else:
				return False

		# Se não estivermos mais no main menu.
		elif(self.page == 1):
			# Caso o objeto game não esteja inicializado.
			if(self.game == 0):
				# Construir um objeto game.
				self.game = in_game(self)
			self.game.start_game()
			return True

		elif(self.page == 2):
			self.start_leaderboards(False)
			return True

		else:
			self.start_rules()
			return True

	def start_leaderboards(self, save, score=0):
		# Caso o objeto não esteja inicializado.
		if(self.leaderboards == 0):
			# Construir um objeto leaderboards.
			self.leaderboards = leaderboards(self, self.screen)
		self.leaderboards.ranking_init(save, score)

	def start_rules(self):
		# Caso o objeto não esteja inicializado.
		if(self.rules == 0):
			# Construir um objeto rules.
			self.rules = rules(self, self.screen)
		self.rules.rules_init()
